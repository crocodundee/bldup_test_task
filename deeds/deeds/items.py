from scrapy import Item, Field


class DeedsItem(Item):
    date = Field()
    type = Field()
    book = Field()
    page_num = Field()
    doc_num = Field()
    city = Field()
    description = Field()
    state = Field()
    street = Field()
    zip = Field()
    cost = Field()



