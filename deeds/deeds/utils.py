from .settings import (
    PAGINATION_EVENT,
    PAGINATION_EVENT_VALUE,
    NEXT_PAGE
)


def get_form_data(response, form_data):
    form_data.update({
        '__VIEWSTATE': get_view_state(response),
    })
    return form_data


def get_view_state(response):
    return response.css('input#__VIEWSTATE::attr(value)').extract_first()


def set_next_page(next_page):
    return {
        PAGINATION_EVENT: PAGINATION_EVENT_VALUE,
        NEXT_PAGE: f'Page${next_page}',
    }
