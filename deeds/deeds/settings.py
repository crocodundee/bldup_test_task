BOT_NAME = 'deeds'

SPIDER_MODULES = ['deeds.spiders']
NEWSPIDER_MODULE = 'deeds.spiders'

USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0'

ROBOTSTXT_OBEY = True
COOKIES_ENABLED = True
COOKIES_DEBUG = True

EXTENSIONS = {
   'scrapy.extensions.telnet.TelnetConsole': None,
}

ITEM_PIPELINES = {
   'deeds.pipelines.DeedsPipeline': 300,
}

# POST DATA
DATE_START = '2020-01-01-00-00-00'
DATE_START_INPUT = 'ctl00$cphMainContent$txtLCSTartDate$dateInput'

DATE_END = '2020-04-10-00-00-00'
DATE_END_INPUT = 'ctl00$cphMainContent$txtLCEndDate$dateInput'

DOC_TYPE = '101627'  # DEED type
DOC_TYPE_INPUT = 'ctl00$cphMainContent$ddlLCDocumentType$vddlDropDown'

SUBMIT = 'ctl00$cphMainContent$btnSearchLC'
SUBMIT_VALUE = 'Search Land Court'

SEARCH_PARAMS = {
   DATE_START_INPUT: DATE_START,
   DATE_END_INPUT: DATE_END,
   DOC_TYPE_INPUT: DOC_TYPE,
   SUBMIT: SUBMIT_VALUE
}

TABLE_RESULTS = 'ctl00_cphMainContent_gvSearchResults'

PAGINATION_EVENT = '__EVENTTARGET'
PAGINATION_EVENT_VALUE = 'ctl00$cphMainContent$gvSearchResults'
NEXT_PAGE = '__EVENTARGUMENT'
