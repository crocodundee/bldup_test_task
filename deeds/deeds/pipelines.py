import re
import json
from datetime import datetime


class DeedsPipeline(object):
    state = 'MA'
    zip = '02780'

    def open_spider(self, spider):
        self.file = open('deeds.jl', 'w')

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        item = self.update_item(item)
        line = json.dumps(item) + "\n"
        self.file.write(line)
        return item

    def update_item(self, item):
        item['date'] = self.get_date(item)
        item['state'] = self.state
        item['zip'] = self.zip
        item['cost'] = self.get_cost(item)
        item['street'] = self.get_street(item)
        for field in item.fields:
            if item[field] == '':
                item[field] = None
        return dict(item)

    def get_date(self, item):
        return datetime.strptime(item['date'], '%m/%d/%Y').date().isoformat()

    def get_cost(self, item):
        cost = re.findall(r'[$](\d*.\d*)', item['description'])
        return float(cost[0].strip('$')) if cost else ''

    def get_street(self, item):
        street = re.findall(r'\d*-\w[\s\w+]*', item['description'])
        return street[0].strip() if street else ''
