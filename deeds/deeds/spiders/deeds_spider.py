from scrapy import Spider, FormRequest
from ..items import DeedsItem
from ..utils import get_form_data, set_next_page
from ..settings import TABLE_RESULTS, SEARCH_PARAMS


class DeedsSpider(Spider):
    name = 'deeds_spider'
    allowed_domains = ['www.tauntondeeds.com']
    start_urls = ['http://www.tauntondeeds.com/Searches/ImageSearch.aspx']
    current_page = 0

    def set_viewstate(self, response, callback, *args, **kwargs):
        formdata = get_form_data(response, *args)
        return FormRequest.from_response(response, formdata=formdata, callback=callback, **kwargs)

    def parse(self, response):
        yield self.set_viewstate(response, self.parse_search_results, SEARCH_PARAMS)

    def parse_search_results(self, response):
        deeds = self.get_search_results(response)

        for deed in deeds:
            deed_info = deed.css('td')
            yield self.get_deed_item(deed_info)

        next_page_num = self.get_next_page(response, self.current_page)
        if next_page_num:
            self.current_page += 1
            yield self.set_viewstate(response,
                                     self.parse_search_results,
                                     set_next_page(next_page_num),
                                     dont_click=True)

    def get_search_results(self, response):
        return response.css(f'table#{TABLE_RESULTS} tr[class*=Row]')

    def get_next_page(self, response, current_page):
        pages = response.css('tr[class=gridPager] a')
        try:
            next_page = pages[current_page + 1]
            return next_page.css('::text').extract_first().strip()
        except IndexError:
            return None

    def get_deed_item(self, doc):
        item = DeedsItem()

        main_details = doc[1:-2].css('::text').extract()
        description = doc[-2].css('span::text').extract_first()
        item['date'] = main_details[0].strip()
        item['type'] = main_details[1].strip()
        item['book'] = main_details[2].strip()
        item['page_num'] = main_details[3].strip()
        item['doc_num'] = main_details[4].strip()
        item['city'] = main_details[5].strip()
        item['description'] = description.strip()

        return item







